FROM ubuntu:22.04 as builder
ENV DEBIAN_FRONTEND noninteractive

RUN apt update && apt install -y curl xxd openssl && rm -rf /var/lib/apt/lists/*

RUN useradd --create-home --shell /bin/bash \
            --user-group tresorit

# Choose to either download the signature checker or copy it from the host:
#COPY check_signature.sh /home/tresorit/
RUN curl -L https://support.tresorit.com/hc/en-us/article_attachments/360009964719 -o /home/tresorit/check_signature.sh

WORKDIR /home/tresorit

# Choose to either download the installer or copy it from the host:
#COPY tresorit_installer.run /home/tresorit/
RUN curl -LO https://installer.tresorit.com/tresorit_installer.run

# Check the signature of the installer and make it executable:
RUN chmod +x /home/tresorit/check_signature.sh && \
    /home/tresorit/check_signature.sh && \
    chmod +x ./tresorit_installer.run

USER tresorit

# The --update-v2 <dir> sets the dest installation dir:
RUN  ./tresorit_installer.run --update-v2 . && \
     rm ./tresorit_installer.run

FROM ubuntu:22.04 as runner
ENV DEBIAN_FRONTEND noninteractive

RUN apt update && apt install -y xxd && rm -rf /var/lib/apt/lists/*

RUN useradd --create-home --shell /bin/bash \
            --user-group tresorit

WORKDIR /home/tresorit

COPY --from=builder /home/tresorit/tresorit* /home/tresorit/

RUN mkdir -p /home/tresorit/Profiles /tresors && \
    chown -R tresorit:tresorit /home/tresorit/Profiles /tresors


COPY start.sh entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/start.sh && \
    chmod +x /usr/local/bin/entrypoint.sh

VOLUME [ "/home/tresorit/Profiles" ]

# USER tresorit

ENTRYPOINT ["entrypoint.sh"]
CMD ["start.sh"]

# Tresorit in a docker

Updated docker image for [Tresorit](https://tresorit.com) based on rootkovska/tresorit-docker. Still only built for x86_64 as Tresorit does not provide ARM binaries. However, the installer is optionally downloaded from the official website during build.

## Build and deploy
You can either build the image yourself or use the pre-built image from the [GitLab Repo](registry.gitlab.com/alexandersalge/tresorit-docker). The version of tresorit-cli in the pre-built image is listed in the [version.txt](https://gitlab.com/alexandersalge/tresorit-docker/-/blob/master/version.txt) file. Default is build yourself. If you want to use the pre-built image, comment out `build: .` and uncomment `image: registry.gitlab.com/alexandersalge/tresorit-docker` in the `docker-compose.yml` file. Then run `docker-compose up -d`.

## Configure each service

```
docker-compose exec tresorit-personal bash
./tresorit-cli status
./tresorit-cli login --email <email> --password <pass>
./tresorit-cli tresors
./tresorit-cli sync --start <tresor_name> --path /vol/<name>
```

Refer to the [official documentation](https://support.tresorit.com/hc/en-us/articles/360009330614-Using-Tresorit-CLI-for-Linux) for more information.
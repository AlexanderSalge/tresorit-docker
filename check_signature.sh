#!/bin/sh

FILENAME=$1
if test "x$FILENAME" = "x"; then
    FILENAME="./tresorit_installer.run"
fi

if test ! -f "$FILENAME"; then
	echo "usage: $0 path/to/tresorit_installer.run"
	exit 1
fi

# Before the signature, the file should always look like "#!/bin/sh\nSIGNATURE="
(echo "#!/bin/sh"; echo -n "SIGNATURE=") > $FILENAME.beginning
if ! (head -c20 $FILENAME | cmp $FILENAME.beginning); then
	echo "Verification Failure"
	rm $FILENAME.beginning
	exit 1
fi
rm $FILENAME.beginning

# After the signature, there should be a single "\n" character that is not yet part of the signed data
echo > $FILENAME.aftersig
if ! (head -c1045 $FILENAME | tail -c1 | cmp $FILENAME.aftersig); then
	echo "Verification Failure"
	rm $FILENAME.aftersig
	exit 1
fi
rm $FILENAME.aftersig

# After the signature, there is the cert chain coded in the CERT_CHAIN="..." form
tail -c+1058 $FILENAME | head -n1 | head -c-2 | sed 's/\\n/\n/g' > $FILENAME.certchain

# Split the cert chain into cert and CA
csplit -z -f $FILENAME.cert $FILENAME.certchain '/-----BEGIN CERTIFICATE-----/' '{*}' >/dev/null
rm $FILENAME.certchain

# Check the certificate is for Code Signing
if ! (openssl x509 -in $FILENAME.cert00 -ext extendedKeyUsage -noout | grep 'Code Signing' >/dev/null); then
	echo "Verification Failure"
    rm $FILENAME.cert*
    exit 1
fi

# We always use the following root ca to sign an intermediate codesign CA which signs the codesign cert actually used
cat >$FILENAME.rootca <<EOF
-----BEGIN CERTIFICATE-----
MIIFcDCCA1igAwIBAgIJANxGW6JljgR5MA0GCSqGSIb3DQEBDQUAMEUxCzAJBgNV
BAYTAkhVMRYwFAYDVQQKDA1UcmVzb3JpdCBLZnQuMR4wHAYDVQQDDBVUcmVzb3Jp
dCBSb290IENBIDIwMTQwHhcNMTQwNDA0MTMzMTExWhcNMjkxMjMxMTMzMTExWjBF
MQswCQYDVQQGEwJIVTEWMBQGA1UECgwNVHJlc29yaXQgS2Z0LjEeMBwGA1UEAwwV
VHJlc29yaXQgUm9vdCBDQSAyMDE0MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIIC
CgKCAgEAkbEAFKBPydLjN7zKortZDJF2NjpvT6JvePTcws7RpWFN0RtF8pFjJ/vD
bcgw5JQMtxcvjQsvpiVl1h4bk11kVEFN4gvzneMBdl9NVurB/nBo96QJIy3kQcWb
dPM8JsMHgpMuvwa1nCIRQjlQ+w89Is+Y55qWC+yXnYG1BHms7T0m6NNKxWgxl0x8
cvP8kOGUDh5CzCNHnMeXFQyad851Fi+hGoistTW4CS0AsI3pPwEM4ycUBK0EOxkU
SjXToWDJ6cpSt62MgIuGL47QChKhG1s/mJrEUK6/OIgXvDhwEVNdEs/6RHDa6jsj
edV89qL7yeomQJra1Br4wxmFCE9R5CSsHTzx0WAw+14zIrUinKMaFYpAqQzUAfDZ
F+icj0/nk9OEkdp4UYS5EwiRR3+i7Yup6PhGaiTXRVmKPuEOXOClhOaxlRiTE1qn
RxX8DaIwRm6cv4koHuOQrU4gUwSKnvX2FePlj8lLz/wtB7J6Af2/LU4gyCfGB6me
usPqsB210sly9fUC4P5HYQjJLLLdc9Tiq89fSnjPhxbfLvJjvJxLt52vWm2Kmo7C
pQgwTPdbKxEWBT0gdNZko/C1cukrmKzicGnrCSEzWp5uBltVEajxpVm385TkvQRf
NgDFRQjErVIJZjG+6Wyoe8i8L+jYxXYQ8uOtH3BT3CxpdY8qttMCAwEAAaNjMGEw
DwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQUO9JAc3c8f2XtGv0xOODRIw1J0bYw
HwYDVR0jBBgwFoAUO9JAc3c8f2XtGv0xOODRIw1J0bYwDgYDVR0PAQH/BAQDAgEG
MA0GCSqGSIb3DQEBDQUAA4ICAQBG2eE8f7ZjYEp5J3wRu2GThhjyoN5lHFk0ia36
3Kj6O7vlxI5IL+B7gxog2P3ng4pJm74upv2w5101wQ+kphg6XVqjc+LahlMam/sR
zTGqWCpXm4ehpsNdLYRfBsGJYmG5I0YuxMoM0lB7kzaF+5lDF1IisrSfr2Bk5APl
gqQPCuByrlXDIWpfs7rvGzoVKjiJfUtupFMKbbL+obh1z0is+WBQTANsm4T+SLjP
AjlTMg08eW/nAIAUFQBmp11OeKZCWaAWx66hNplzzbKWYu6pVTCAXqA7XHkmRkWn
uOufq/uM0aSHya0p7/Ha3lEsr+Om0IU4QswkUkksi+VQ9OMk9CWvQRexirr1TJfD
/jbnwNSg7ESFFGeD0kpX/qy92iHOHymwau/DBx07Dfbb4PwyNpiQRRAErwqqvFSa
1/t6kQrnF2Im9Kk7CvLv8xkjhp08vy+8t8FIKBEwpz5hMvF/cKM5uXwwlrhSTxHe
eXwNFPn+/wektcM2aPa1WQxMYQB5Itjwe0gUS8bcB7Y1U4I7d/b9ODmzlY48ZIu9
Vr76eirxrRtY9cqedcJTZR4+tIG+rcdUd7hqCPK4vyXnTlVe0MTOVzRHQayCVZfm
3JHZ92LFRUUuaX5Th36mHqR4hTrTL9H9MTKpz0iCAcLXW+El/DH+kyRmHR2vspxU
5bzRIA==
-----END CERTIFICATE-----
EOF

# We can verify the chain with OpenSSL
if ! (openssl verify -CAfile $FILENAME.rootca -untrusted $FILENAME.cert01 $FILENAME.cert00 >/dev/null); then
	echo "Verification Failure"
    rm $FILENAME.cert*
    rm $FILENAME.rootca
    exit 1
fi
rm $FILENAME.rootca

# Export the public key from the certificate
openssl x509 -in $FILENAME.cert00 -pubkey -noout > $FILENAME.pubkey
rm $FILENAME.cert*

# The signature is always at character position 21..1044 in hex form
head -c1044 $FILENAME | tail -c+20 | xxd -r -p > $FILENAME.signature

# We have to check the validity of the rest of the file
# This openssl call will print either "Verified OK" or "Verification Failure"
tail -c+1046 $FILENAME | openssl sha512 -verify $FILENAME.pubkey -sigopt rsa_padding_mode:pss -sigopt rsa_pss_saltlen:-1 -signature $FILENAME.signature
EXITCODE=$?

rm $FILENAME.signature
rm $FILENAME.pubkey
exit $EXITCODE

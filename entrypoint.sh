#!/bin/sh

set -e

# Set the UID and GID based on environment variables (default to 1000 if not set)
PUID=${PUID:-1000}
PGID=${PGID:-1000}

# Change the UID and GID of the user inside the container
if [ "$PUID" -ne 1000 ] || [ "$PGID" -ne 1000 ]; then
    echo "Changing UID and GID to $PUID:$PGID"
    sed -i -e "s/tresorit:x:1000:1000/tresorit:x:$PUID:$PGID/" /etc/passwd
    sed -i -e "s/tresorit:x:1000:/tresorit:x:$PGID:/" /etc/group
    chown -R $PUID:$PGID /home/tresorit
fi

# Start the Tresorit client as the user inside the container
su - tresorit -c "/home/tresorit/tresorit-cli start"
su - tresorit -c "/home/tresorit/tresorit-cli status"

exec "$@"
